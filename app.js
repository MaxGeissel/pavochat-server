if (!Array.prototype.includes) {
  Array.prototype.includes = function(searchElement /*, fromIndex*/ ) {
    'use strict';
    var O = Object(this);
    var len = parseInt(O.length, 10) || 0;
    if (len === 0) {
      return false;
    }
    var n = parseInt(arguments[1], 10) || 0;
    var k;
    if (n >= 0) {
      k = n;
    } else {
      k = len + n;
      if (k < 0) {k = 0;}
    }
    var currentElement;
    var searchIsNaN = isNaN(searchElement);
    while (k < len) {
      currentElement = O[k];
      if (searchElement === currentElement || (searchIsNaN && isNaN(currentElement))) {
        return true;
      }
      k++;
    }
    return false;
  };
}

var express = require("express");
var path = require("path");
var app = express();

var server = require("http").Server(app);
var io = require("socket.io").listen(server);

var rooms = [];

io.on("connection", function(socket){
    socket.on("getUpdatedChatRooms", function() {
      io.sockets.emit("updateChatRooms", rooms);
    });

    socket.on("joinChatRoom", function(data){
      socket.userName = data.userName.substring(0,30);
      socket.room = data.chatRoom.substring(0,30);
      if (!rooms.includes(socket.room)) {
        var room = {
          roomName : socket.room,
          userNames: []
        };
        rooms.push(room);
        io.sockets.in(socket.room).emit("updateChatRooms", rooms);
      }
      var chatRoom = getRoomByName(socket.room);
      chatRoom.userNames.push(socket.userName);
      io.sockets.in(socket.room).emit('updateChat', 'Server', socket.userName + " hat den Chatraum betreten.", new Date());
      socket.join(socket.room);
      io.sockets.in(socket.room).emit("updateUsers", chatRoom.userNames);
    })

    socket.on("sendMessage", function(msg){
        if (msg.message.text.length > 0 && msg.message.text.length < 1000) {
          io.sockets.in(socket.room).emit('updateChat', socket.userName, msg.message.text, msg.message.date);
        }
    });

    socket.on('leaveRoom', function(){
      socket.broadcast.to(socket.room).emit("updateChat", 'Server', socket.userName + " hat den Chatraum verlassen.", new Date());
      var chatRoom = getRoomByName(socket.room);
      socket.leave(socket.room);
      if (chatRoom.userNames != null) {
        var index = chatRoom.userNames.indexOf(socket.userName);
        if (index !== -1) {
          chatRoom.userNames.splice(index,1);
          if (chatRoom.userNames.length <= 0) {
            deleteRoom(chatRoom);
            io.sockets.in(socket.room).emit("updateChatRooms", rooms);
          }
          io.sockets.in(socket.room).emit("updateUsers", chatRoom.userNames);
        }
      }
  	});

    socket.on('disconnect', function() {
      socket.broadcast.to(socket.room).emit("updateChat", "Server", socket.userName + " hat den Chatraum verlassen.", new Date());
      var chatRoom = getRoomByName(socket.room);
      socket.leave(socket.room);
      if (chatRoom.userNames != null) {
        var index = chatRoom.userNames.indexOf(socket.userName);
        if (index !== -1) {
          chatRoom.userNames.splice(index,1);
          if (chatRoom.userNames.length <= 0) {
            deleteRoom(chatRoom);
            io.sockets.in(socket.room).emit("updateChatRooms", rooms);
          }
          io.sockets.in(socket.room).emit("updateUsers", chatRoom.userNames);
        }
      }
    });
});

deleteRoom = function(chatRoom) {
  var index = rooms.indexOf(chatRoom);
  if (index !== -1) {
    rooms.splice(index,1);
  }
}

getRoomByName = function(string) {
  for (var i = 0; i < rooms.length; i++){
    if (rooms[i].roomName === string) {
      return rooms[i];
    }
  }
  return [];
}

server.listen(3001, function () {
    console.log("Listening on port %s...", server.address().port);
});
